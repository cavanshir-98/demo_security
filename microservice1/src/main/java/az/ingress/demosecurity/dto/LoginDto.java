package az.ingress.demosecurity.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(exclude = "password")
public class LoginDto {

    private String email;

    private String password;



}
