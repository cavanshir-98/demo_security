package az.ingress.demosecurity.controller;

import az.ingress.demosecurity.dto.LoginResponseDto;
import az.ingress.demosecurity.dto.RegistrationDto;
import az.ingress.demosecurity.model.User;
import az.ingress.demosecurity.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/security")
public class UserController {

    private final UserServiceImpl userService;

    @PostMapping("/registration")
    public void register(@RequestBody User request) {
        userService.register(request);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> login(@RequestBody @Valid RegistrationDto request) {
        return ResponseEntity.ok(userService.login(request));
    }


}
