package az.ingress.demosecurity.service;

import az.ingress.demosecurity.config.jwt.JwtService;
import az.ingress.demosecurity.dto.JwtCredentials;
import az.ingress.demosecurity.dto.LoginResponseDto;
import az.ingress.demosecurity.dto.RegistrationDto;
import az.ingress.demosecurity.exception.UserNamePasswordValidExcept;
import az.ingress.demosecurity.model.Authority;
import az.ingress.demosecurity.model.Role;
import az.ingress.demosecurity.model.User;
import az.ingress.demosecurity.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JwtService jwtService;

    public void register(User request) {
        var user = userRepository.findByUsername(request.getUsername());
        if (user.isPresent()) {
            throw new RuntimeException("Email already taken");
        }
        log.info("Trying to create user");
        Authority authority = new Authority();
        authority.setRole(Role.ROLE_USER);
        User user1 = User.builder()
                .username(request.getUsername())
                .password(bCryptPasswordEncoder.encode(request.getPassword()))
                .isAccountNonExpired(true)
                .isAccountNonLocked(true)
                .credentialsNonExpired(true)
                .isEnabled(true)
                .authorities(List.of(authority))
                .build();
        userRepository.save(user1);

    }

    public LoginResponseDto login(RegistrationDto request) {
        var userDetails = (User) loadUserByUsername(request.getEmail());
        boolean matches = bCryptPasswordEncoder.matches(request.getPassword(), userDetails.getPassword());
        if (!matches) {
            throw new UserNamePasswordValidExcept();
        }
        return LoginResponseDto.builder()
                .jwt(jwtService.issueToken(JwtCredentials.builder()
                        .role(userDetails.getAuthorities().stream().map(Authority::getRole).toList())
                        .email(userDetails.getUsername())
                        .build()))
                .build();

    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.info("Getting user information by user name {}", username);
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + username + " not found"));
        return user;
    }
}
