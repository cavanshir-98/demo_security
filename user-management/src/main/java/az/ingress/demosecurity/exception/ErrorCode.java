package az.ingress.demosecurity.exception;

public enum ErrorCode {

    MS9_BAD_REQUEST_001,
    MS9_BAD_REQUEST_002,
    MS9_BAD_REQUEST_003

}
