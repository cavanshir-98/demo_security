package az.ingress.demosecurity.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.time.OffsetDateTime;
import java.util.Objects;

import static az.ingress.demosecurity.exception.ErrorCode.*;

@RestControllerAdvice
@Slf4j
public class UserMenExceptionHandler {

    @ExceptionHandler(UserNamePasswordValidExcept.class)
    public ResponseEntity<ErrorResponseDto>handleInvalidUserNamePasswordException(UserNamePasswordValidExcept ex,
                                                                                  WebRequest webRequest){
        String language = webRequest.getHeader("Accept-Language");
        log.info("Request language is {}",language);
        var path =((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception",ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                        .status(400)
                        .code(MS9_BAD_REQUEST_001.toString())
                        .message("Bad request")
                        .detail("Username or password")
                        .timestamp(OffsetDateTime.now())
                        .path(path)
                .build());

    }
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ErrorResponseDto>httpMessageNotReadableException(HttpMessageNotReadableException ex,
                                                                           WebRequest webRequest){
        var path =((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception",ex.getLocalizedMessage());
        ex.printStackTrace();
        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_002.toString())
                .message("Bad request")
                .detail(ex.getLocalizedMessage())
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorResponseDto>methodArgumentNotValidException(MethodArgumentNotValidException ex,
                                                                           WebRequest webRequest){
        var path =((ServletWebRequest) webRequest).getRequest().getRequestURL().toString();
        log.error("Exception",ex.getLocalizedMessage());
        ex.printStackTrace();

        String detail="";
FieldError fieldError=ex.getBindingResult().getFieldError();
           detail= detail.concat(fieldError.getField().concat("-").concat(Objects.requireNonNull(fieldError.getDefaultMessage())));


        return ResponseEntity.status(400).body(ErrorResponseDto.builder()
                .status(400)
                .code(MS9_BAD_REQUEST_003.toString())
                .message("Bad request")
                .detail(detail)
                .timestamp(OffsetDateTime.now())
                .path(path)
                .build());

    }
}
