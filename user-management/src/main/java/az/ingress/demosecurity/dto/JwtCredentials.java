package az.ingress.demosecurity.dto;

import java.util.List;

import az.ingress.demosecurity.model.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JwtCredentials {

    String email;
    List<Role> role;
    Long id;
}
