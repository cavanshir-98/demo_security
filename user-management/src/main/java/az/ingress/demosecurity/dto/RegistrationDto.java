package az.ingress.demosecurity.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationDto {


    @NotBlank
    private String email;

    @NotBlank
    @Size(min=5,max = 10)
    private String password;

}
